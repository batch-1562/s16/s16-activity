let myString = "supercalifragilisticexpialidocious"
console.log(myString);
let filteredString = ''
for(let x=0; x<myString.length; x++){
	if(
		myString[x] == "a" ||
		myString[x] == "e" ||
		myString[x] == "i" ||
		myString[x] == "o" ||
		myString[x] == "u" 
	){
		continue;
	} else {		
		filteredString += myString[x];
	}	
}
console.log(filteredString);
